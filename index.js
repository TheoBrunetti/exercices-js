const a = "1"
const b = 1
const c = true
const d = {nom:"theo"}
const e = [1,2,3]
// Exercice Numéro 1 Début//
const tab = [1,2,3,4,5,6,7,8,9,10]

function showEvenNumber(myTab){
    // Compléter la fonction
    let myEvenTab = []
for (let index = 0; index < myTab.length; index++) {

    const element = myTab[index];
    if (element % 2 === 0 ) {
        myEvenTab.push(element)
    }

} 
return myEvenTab
}

const result= showEvenNumber(tab)
console.log('====================================');
console.log(result);
console.log('====================================');


function showOddNumber(myTab){
    // Compléter la fonction
    let myOddTab = []
    for (let index = 0; index < myTab.length; index++) {
        
        const element = myTab[index];
        if (element % 2 === 1) {
            myOddTab.push(element)
        }
    }
    return myOddTab
}

const resultat= showOddNumber(tab)
console.log('====================================');
console.log(resultat);
console.log('====================================');


//Exercice Numéro 1 Fin //

//Exercice Numéro 2 Début//

let myLower = "abcdef"
let myUpper = "ABCDEF"

function ShowLowerUpper(myLower, myUpper){
    // Compléter la fonction

    return myLower,myUpper
}

ShowLowerUpper(myLower, myUpper);
console.log(myLower, myUpper);

